<?php

namespace Drupal\typescript_generator\Exception;

/**
 * Custom exception when a generator is not found for a given type.
 */
class MissingGeneratorException extends \RuntimeException {}
